﻿using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;
using eBibliotheque.Models;

namespace eBibliotheque
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<LibraryContext>());
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
