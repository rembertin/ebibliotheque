namespace eBibliotheque.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.People",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        Firstname = c.String(),
                        Lastname = c.String(nullable: false),
                        Birthdate = c.DateTime(),
                        Death = c.DateTime(),
                        Email = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        BookId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Year = c.Short(nullable: false),
                        AuthorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BookId)
                .ForeignKey("dbo.People", t => t.AuthorId, cascadeDelete: true)
                .Index(t => t.AuthorId);
            
            CreateTable(
                "dbo.Borrowings",
                c => new
                    {
                        BorrowingId = c.Int(nullable: false, identity: true),
                        BeginningDate = c.DateTime(nullable: false),
                        EndDatePlanned = c.DateTime(nullable: false),
                        EndDateReal = c.DateTime(),
                        Book_BookId = c.Int(),
                        Member_PersonId = c.Int(),
                    })
                .PrimaryKey(t => t.BorrowingId)
                .ForeignKey("dbo.Books", t => t.Book_BookId)
                .ForeignKey("dbo.People", t => t.Member_PersonId)
                .Index(t => t.Book_BookId)
                .Index(t => t.Member_PersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Borrowings", "Member_PersonId", "dbo.People");
            DropForeignKey("dbo.Borrowings", "Book_BookId", "dbo.Books");
            DropForeignKey("dbo.Books", "AuthorId", "dbo.People");
            DropIndex("dbo.Borrowings", new[] { "Member_PersonId" });
            DropIndex("dbo.Borrowings", new[] { "Book_BookId" });
            DropIndex("dbo.Books", new[] { "AuthorId" });
            DropTable("dbo.Borrowings");
            DropTable("dbo.Books");
            DropTable("dbo.People");
        }
    }
}
