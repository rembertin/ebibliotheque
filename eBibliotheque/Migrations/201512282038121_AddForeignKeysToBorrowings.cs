namespace eBibliotheque.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeysToBorrowings : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Borrowings", "Book_BookId", "dbo.Books");
            DropForeignKey("dbo.Borrowings", "Member_PersonId", "dbo.People");
            DropIndex("dbo.Borrowings", new[] { "Book_BookId" });
            DropIndex("dbo.Borrowings", new[] { "Member_PersonId" });
            RenameColumn(table: "dbo.Borrowings", name: "Book_BookId", newName: "BookId");
            RenameColumn(table: "dbo.Borrowings", name: "Member_PersonId", newName: "MemberId");
            AlterColumn("dbo.Borrowings", "BookId", c => c.Int(nullable: false));
            AlterColumn("dbo.Borrowings", "MemberId", c => c.Int(nullable: false));
            CreateIndex("dbo.Borrowings", "BookId");
            CreateIndex("dbo.Borrowings", "MemberId");
            AddForeignKey("dbo.Borrowings", "BookId", "dbo.Books", "BookId");
            AddForeignKey("dbo.Borrowings", "MemberId", "dbo.People", "PersonId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Borrowings", "MemberId", "dbo.People");
            DropForeignKey("dbo.Borrowings", "BookId", "dbo.Books");
            DropIndex("dbo.Borrowings", new[] { "MemberId" });
            DropIndex("dbo.Borrowings", new[] { "BookId" });
            AlterColumn("dbo.Borrowings", "MemberId", c => c.Int());
            AlterColumn("dbo.Borrowings", "BookId", c => c.Int());
            RenameColumn(table: "dbo.Borrowings", name: "MemberId", newName: "Member_PersonId");
            RenameColumn(table: "dbo.Borrowings", name: "BookId", newName: "Book_BookId");
            CreateIndex("dbo.Borrowings", "Member_PersonId");
            CreateIndex("dbo.Borrowings", "Book_BookId");
            AddForeignKey("dbo.Borrowings", "Member_PersonId", "dbo.People", "PersonId");
            AddForeignKey("dbo.Borrowings", "Book_BookId", "dbo.Books", "BookId");
        }
    }
}
