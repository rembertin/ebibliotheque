using System.Collections.Generic;
using System.IO;
using System.Linq;
using eBibliotheque.Models;
using Faker;

namespace eBibliotheque.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<LibraryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "LibraryManager.Models.LibraryContext";
        }

        protected override void Seed(LibraryContext context)
        {
            // Cleaning database
            context.Database.ExecuteSqlCommand("delete from Borrowings; DBCC CHECKIDENT (Borrowings, RESEED, 0)");
            context.Database.ExecuteSqlCommand("delete from Books; DBCC CHECKIDENT (Books, RESEED, 0)");
            context.Database.ExecuteSqlCommand("delete from People; DBCC CHECKIDENT (People, RESEED, 0)");

            var random = new Random();

            // Authors seeding
            for (var i = 1; i <= 10; i++)
            {
                var author = new Author
                {
                    Firstname = Name.First(),
                    Lastname = Name.Last(),
                    Birthdate = DateTime.Today.AddYears(random.Next(-100,-20))
                        .AddMonths(random.Next(0, 11)).AddDays(random.Next(0, 30))
                };
                if (random.Next(0, 10) >= 2)
                {
                    author.Death = author.Birthdate.AddYears(random.Next(20, 80))
                        .AddMonths(random.Next(0, 11)).AddDays(random.Next(0, 30));
                }
                context.Authors.AddOrUpdate(a => a.PersonId, author);
            }
            context.SaveChanges();

            // Books seeding
            foreach (var author in context.Authors.ToList())
            {
                int booksNumber = random.Next(1, 4);
                for (int i = 0; i < booksNumber; i++)
                {
                    context.Books.AddOrUpdate(b=>b.BookId, new Book
                    {
                        AuthorId = author.PersonId, 
                        Title = Lorem.Sentence(),
                        Year = (short)random.Next(author.Birthdate.Year + 15, author.Death?.Year ?? DateTime.Today.Year)
                    });
                }
            }

            // Members seeding
            for (int i = 0; i < 5; i++)
            {
                var member = new Member
                {
                    Firstname = Name.First(),
                    Lastname = Name.Last()
                };
                member.Email = Internet.Email(member.FullName);
                context.Members.AddOrUpdate(m=>m.PersonId,
                    member);
            }
            context.SaveChanges();

            // Borrowings seeding
            var books = context.Books.ToList();
            foreach (var member in context.Members.ToList())
            {
                var borrowingsNumber = random.Next(1, 3);
                for (int j = 0; j < borrowingsNumber; j++)
                {
                    var borrowing = new Borrowing
                    {
                        MemberId = member.PersonId,
                        BookId = books[random.Next(books.Count)].BookId,
                        BeginningDate = DateTime.Today.AddYears(random.Next(-1, 0))
                        .AddMonths(random.Next(-11, 0)).AddDays(random.Next(-30,0))
                    };
                    borrowing.EndDatePlanned = borrowing.BeginningDate.AddDays(random.Next(0, 365));
                    if (DateTime.Today > borrowing.EndDatePlanned && random.Next(0,10)>3)
                    {
                        borrowing.EndDateReal = borrowing.EndDatePlanned.AddDays(random.Next(-2, 2));
                    }
                    context.Borrowings.AddOrUpdate(b=>b.BorrowingId, borrowing);
                }
                
            }
        }
    }
}
