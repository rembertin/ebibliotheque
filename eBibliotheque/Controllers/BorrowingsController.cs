﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eBibliotheque.Models;

namespace eBibliotheque.Controllers
{
    public class BorrowingsController : Controller
    {
        private readonly LibraryContext _db = new LibraryContext();

        // GET: Borrowings
        public ActionResult Index()
        {
            var borrowings = _db.Borrowings.Include(b => b.Book)
                .Include(b => b.Member).OrderByDescending(b=>b.BeginningDate);
            return View(borrowings.ToList());
        }

        // GET: Borrowings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Borrowing borrowing = _db.Borrowings.Find(id);
            if (borrowing == null)
            {
                return HttpNotFound();
            }
            return View(borrowing);
        }

        // GET: Borrowings/Create
        public ActionResult Create()
        {
            ViewBag.BookId = new SelectList(_db.Books, "BookId", "TitleAndAuthor");
            ViewBag.MemberId = new SelectList(_db.Members, "PersonId", "FullName");
            return View();
        }

        // POST: Borrowings/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BorrowingId,BeginningDate,EndDatePlanned,EndDateReal,BookId,MemberId")] Borrowing borrowing)
        {
            if (ModelState.IsValid)
            {
                _db.Borrowings.Add(borrowing);
                _db.SaveChanges();
                return RedirectToAction("Details", new { id = borrowing.BorrowingId });
            }

            ViewBag.BookId = new SelectList(_db.Books, "BookId", "Title", borrowing.BookId);
            ViewBag.MemberId = new SelectList(_db.Members, "PersonId", "FullName", borrowing.MemberId);
            return View(borrowing);
        }

        // GET: Borrowings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Borrowing borrowing = _db.Borrowings.Find(id);
            if (borrowing == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookId = new SelectList(_db.Books, "BookId", "Title", borrowing.BookId);
            ViewBag.MemberId = new SelectList(_db.Members, "PersonId", "FullName", borrowing.MemberId);
            return View(borrowing);
        }

        // POST: Borrowings/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BorrowingId,BeginningDate,EndDatePlanned,EndDateReal,BookId,MemberId")] Borrowing borrowing)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(borrowing).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", new { id = borrowing.BorrowingId });
            }
            ViewBag.BookId = new SelectList(_db.Books, "BookId", "Title", borrowing.BookId);
            ViewBag.MemberId = new SelectList(_db.Members, "PersonId", "FullName", borrowing.MemberId);
            return View(borrowing);
        }

        // GET: Borrowings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Borrowing borrowing = _db.Borrowings.Find(id);
            if (borrowing == null)
            {
                return HttpNotFound();
            }
            return View(borrowing);
        }

        // POST: Borrowings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Borrowing borrowing = _db.Borrowings.Find(id);
            _db.Borrowings.Remove(borrowing);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
