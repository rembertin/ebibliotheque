﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBibliotheque.Models;
using eBibliotheque.Models.ViewModels;

namespace eBibliotheque.Controllers
{
    public class HomeController : Controller
    {
        private readonly LibraryContext _db = new LibraryContext();

        // GET: Home
        public ActionResult Index()
        {
            return View(new TopFiveAllResourcesViewModel(_db));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}