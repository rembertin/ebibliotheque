﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBibliotheque.Models
{
    public abstract class Person
    {
        public int PersonId { get; set; }

        [Display(Name = "Prénom")]
        public string Firstname { get; set; }

        [Required, Display(Name = "Nom")]
        public string Lastname { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                var fullName = Lastname;
                if (! string.IsNullOrWhiteSpace(Firstname))
                {
                    fullName = Firstname + " " + fullName;
                }
                return fullName;
            }
        }
    }
}