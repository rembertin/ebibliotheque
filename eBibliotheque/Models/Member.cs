﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace eBibliotheque.Models
{
    public class Member : Person
    {
        [DataType(DataType.EmailAddress), Display(Name = "Adresse email")]
        public string Email { get; set; }

        [Display(Name = "Emprunts réalisés")]
        public virtual ICollection<Borrowing> Borrowings { get; set; }

        public void DeleteBorrowings(LibraryContext db)
        {
            db.Borrowings.RemoveRange(Borrowings);
        }
    }
}