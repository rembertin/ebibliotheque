﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBibliotheque.Models
{
    public class Borrowing
    {
        public int BorrowingId { get; set; }

        [DataType(DataType.Date), Required, Display(Name = "Date de début")]
        public DateTime BeginningDate { get; set; }

        [DataType(DataType.Date), Required, Display(Name = "Date de fin prévue")]
        public DateTime EndDatePlanned { get; set; }

        [DataType(DataType.Date), Display(Name = "Date de fin réelle")]
        public DateTime? EndDateReal { get; set; }

        [Display(Name = "Livre emprunté"), ForeignKey("BookId")]
        public virtual Book Book { get; set; }
        public int BookId { get; set; }

        [Display(Name = "Membre"), ForeignKey("MemberId")]
        public virtual Member Member { get; set; }
        public int MemberId { get; set; }

        [NotMapped]
        public string MemberAndDates => $"{Member.FullName} ({BeginningDate.ToString("d")} - {EndDateString})";

        [NotMapped]
        public string EndDateString => $"{EndDateReal?.ToString("d") ?? EndDatePlanned.ToString("d")}";

        public override string ToString()
        {
            return
                $"{BeginningDate.ToString("d")} - {EndDateString} : " +
                $"{Book.Title} ({Book.Author.FullName})";
        }
    }
}