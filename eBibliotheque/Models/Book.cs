﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBibliotheque.Models
{
    public class Book
    {
        public int BookId { get; set; }

        [Required, MinLength(3), Display(Name = "Titre du livre")]
        public string Title { get; set; }

        [Range(0, 2050), Display(Name = "Année de parution")]
        public short Year { get; set; }

        [Display(Name = "Auteur"), ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }

        [Required]
        public virtual int AuthorId { get; set; }

        [Display(Name = "Emprunts du livre")]
        public virtual ICollection<Borrowing> Borrowings { get; set; }

        [NotMapped]
        public string TitleAndAuthor => Title + " (" + Author.FullName + ")";

        public void DeleteBorrowings(LibraryContext db)
        {
            db.Borrowings.RemoveRange(Borrowings);
        }
    }
}