﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBibliotheque.Models.ViewModels
{
    public class TopFiveAllResourcesViewModel
    {
        public TopFiveAllResourcesViewModel(LibraryContext context)
        {
            Books = context.Books.Take(5).ToList();
            Authors = context.Authors.Take(5).ToList();
            Members = context.Members.Take(5).ToList();
            Borrowings = context.Borrowings.OrderByDescending(b=>b.BeginningDate).Take(5).ToList();
        }

        public ICollection<Book> Books { get; set; } 
        public ICollection<Author> Authors { get; set; } 
        public ICollection<Member> Members { get; set; } 
        public ICollection<Borrowing> Borrowings { get; set; }
    }
}