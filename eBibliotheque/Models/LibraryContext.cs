﻿using System.Data.Entity;

namespace eBibliotheque.Models
{
    public class LibraryContext : DbContext
    {
        public DbSet<Author> Authors { get; set; } 
        public DbSet<Book> Books { get; set; } 
        public DbSet<Borrowing> Borrowings { get; set; } 
        public DbSet<Member> Members { get; set; } 
        public DbSet<Person> Persons { get; set; }
    }

    
}