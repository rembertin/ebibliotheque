﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eBibliotheque.Models
{
    public class Author : Person
    {
        [DataType(DataType.Date), Display(Name = "Date de naissance"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }

        [DataType(DataType.Date), Display(Name = "Date de décès"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Death { get; set; }

        [Display(Name = "Livres écrits")]
        public virtual ICollection<Book> BooksWritten { get; set; }

        public void DeleteBooks(LibraryContext db)
        {
            foreach (var book in BooksWritten)
            {
                book.DeleteBorrowings(db);
            }
            db.Books.RemoveRange(BooksWritten);
        }
    }
}