# e-Bibliotheque
This project is a small website useful to manage a library. It allows to show/add/edit/delete some books, authors, library members, and manage members borrowings.

## Technologies and requirements
The project was realised in C# with ASP.NET MVC 5, using Entity Framework 6.
It requires the .NET framework 4.5.2, SQL Server or MS LocalDB (you can change the connection in the Web.config file). To run it, you should have Visual Studio installed (2012 or further version) with the package manager NuGet.

## Installation
Go to the directory that will contain the repo, and run :
```sh
$ git clone https://rembertin@bitbucket.org/rembertin/ebibliotheque.git eBibliotheque
```
Note : if you don't have git, you can simply download the repo by following this link : https://bitbucket.org/rembertin/ebibliotheque/get/867f6e07b823.zip

## Configuration
Open the solution file (eBibliothque.sln) in Visual Studio. Then, open the Package Manager Console (Tools > Library Package Manager > Package Manager Console). It will ask you to restore the package : say yes. Finally, run this command into the console :
```sh
PM> Update-Database
```
It will migrate the database and populate it with random data.

There you're ready to go and run the app!